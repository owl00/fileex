package com.netty.my;

import com.netty.http.HttpServerHandler;
import com.netty.utils.Constants;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelException;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.multipart.*;
import java.io.IOException;
import java.nio.charset.Charset;
import java.io.File;
public class MyDiskFileUpload extends MyAbstractDiskHttpData implements FileUpload {

    public static String baseDirectory= Constants.BASE_DIRECTORY;
    public static boolean deleteOnExitTemporaryFile = false;
    public static final String prefix = "FUp_";
    public static final String postfix = "";
    private String filename;
    private String contentType;
    private String contentTransferEncoding;
    private HttpServerHandler httpServerHandler;
    public MyDiskFileUpload(String name, String filename, String contentType, String contentTransferEncoding, Charset charset, long size,HttpServerHandler httpServerHandler) {
        super(name, charset, size);
        this.setFilename(filename);
        this.setContentType(contentType);
        this.setContentTransferEncoding(contentTransferEncoding);
        this.httpServerHandler=httpServerHandler;
    }

    @Override
    public HttpServerHandler getHttpServerHandler() {
        return httpServerHandler;
    }

    public void setHttpServerHandler(HttpServerHandler httpServerHandler) {
        this.httpServerHandler = httpServerHandler;
    }

    @Override
    public HttpDataType getHttpDataType() {
        return HttpDataType.FileUpload;
    }
    @Override
    public String getFilename() {
        return this.filename;
    }
    @Override
    public void setFilename(String filename) {
        if (filename == null) {
            throw new NullPointerException("filename");
        } else {
            this.filename = filename;
        }
    }
    @Override
    public int hashCode() {
          return this.getName().hashCode();
    }
    @Override
    public boolean equals(Object o) {
        return o instanceof FileUpload &&
                this.getName().equalsIgnoreCase(((FileUpload)o).getName());
    }
    @Override
    public int compareTo(InterfaceHttpData o) {
        if (!(o instanceof FileUpload)) {
            throw new ClassCastException("Cannot compare " + this.getHttpDataType() + " with " + o.getHttpDataType());
        } else {
            return this.compareTo((FileUpload)o);
        }
    }

    public int compareTo(FileUpload o) {
        return this.getName().compareToIgnoreCase(o.getName());
    }
    @Override
    public void setContentType(String contentType) {
        if (contentType == null) {
            throw new NullPointerException("contentType");
        } else {
            this.contentType = contentType;
        }
    }
    @Override
    public String getContentType() {
        return this.contentType;
    }
    @Override
    public String getContentTransferEncoding() {
        return this.contentTransferEncoding;
    }
    @Override
    public void setContentTransferEncoding(String contentTransferEncoding) {
        this.contentTransferEncoding = contentTransferEncoding;
    }
    @Override
    public String toString() {
        File file = null;

        try {
            file = this.getFile();
        } catch (IOException var3) {
            ;
        }

        return HttpHeaderNames.CONTENT_DISPOSITION + ": " + HttpHeaderValues.FORM_DATA + "; " + HttpHeaderValues.NAME + "=\"" + this.getName() + "\"; " + HttpHeaderValues.FILENAME + "=\"" + this.filename + "\"\r\n" + HttpHeaderNames.CONTENT_TYPE + ": " + this.contentType + (this.getCharset() != null ? "; " + HttpHeaderValues.CHARSET + '=' + this.getCharset().name() + "\r\n" : "\r\n") + HttpHeaderNames.CONTENT_LENGTH + ": " + this.length() + "\r\nCompleted: " + this.isCompleted() + "\r\nIsInMemory: " + this.isInMemory() + "\r\nRealFile: " + (file != null ? file.getAbsolutePath() : "null") + " DefaultDeleteAfter: " + deleteOnExitTemporaryFile;
    }
    @Override
    protected boolean deleteOnExit() {
        return deleteOnExitTemporaryFile;
    }
    @Override
    protected String getBaseDirectory() {
        return baseDirectory;
    }
    @Override
    protected String getDiskFilename() {
        return "upload";
    }
    @Override
    protected String getPostfix() {
        return postfix;
    }
    @Override
    protected String getPrefix() {
        return prefix;
    }
    @Override
    public FileUpload copy() {
        ByteBuf content = this.content();
        return this.replace(content != null ? content.copy() : null);
    }
    @Override
    public FileUpload duplicate() {
        ByteBuf content = this.content();
        return this.replace(content != null ? content.duplicate() : null);
    }
    @Override
    public FileUpload retainedDuplicate() {
        ByteBuf content = this.content();
        if (content != null) {
            content = content.retainedDuplicate();
            boolean success = false;

            FileUpload var4;
            try {
                FileUpload duplicate = this.replace(content);
                success = true;
                var4 = duplicate;
            } finally {
                if (!success) {
                    content.release();
                }

            }

            return var4;
        } else {
            return this.replace((ByteBuf)null);
        }
    }
    @Override
    public FileUpload replace(ByteBuf content) {
        DiskFileUpload upload = new DiskFileUpload(this.getName(), this.getFilename(), this.getContentType(), this.getContentTransferEncoding(), this.getCharset(), this.size);
        if (content != null) {
            try {
                upload.setContent(content);
            } catch (IOException var4) {
                throw new ChannelException(var4);
            }
        }

        return upload;
    }
    @Override
    public FileUpload retain(int increment) {
        super.retain(increment);
        return this;
    }
    @Override
    public FileUpload retain() {
        super.retain();
        return this;
    }
    @Override
    public FileUpload touch() {
        super.touch();
        return this;
    }
    @Override
    public FileUpload touch(Object hint) {
        super.touch(hint);
        return this;
    }
}
